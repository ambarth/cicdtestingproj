#!/bin/bash
echo "installing node"
curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
chmod +x nodesource_setup.sh
./nodesource_setup.sh
apt-get install nodejs
echo "... done, installed versions: "
echo "node version: " + $(node -v)
echo "npm version:  " + $(npm -v)

echo "installing jq"
apt-get install jq
echo "... done"

echo "installing docker"
curl -sL http://get.docker.com -o install_docker.sh
chmod +x install_docker.sh
./install_docker.sh
echo "... done -> " + $(docker -v)

echo "setting up boulderbook environment"
alias bb=$BOULDERBOOK/boulderbook.sh
echo "... done"

